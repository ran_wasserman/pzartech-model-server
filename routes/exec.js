var express = require('express');
var exec = require('child_process').exec;
var generator = require('../modules/models_generator');
var router = express.Router();

var parseResponse = function(execResponse){
    response = {
        id : '123456789',
        types : ['jpg','stl']
    };
};

/* GET users listing. */
router.get('/', function(req, res, next) {
    var isWin = /^win/.test(process.platform);
    console.log('isWin %s', isWin);
    var response = res;

    var command;

    if(isWin){
        command = 'dir';
    } else {
        command = 'ls -l';
    }

    var child = exec(command, function(error, stdout, stderr){
        if(error){
            console.log('error: ' + error);
        }
        console.log('stdout: ' + stdout);
        console.log('stderr: ' + stderr);
        var json = {error : error, stdout: stdout, stderr: stderr};
        response.send(JSON.stringify(json));
    });

});

router.get('/generate', function(req, res, next) {
   
    var modelId = req.query.modelId;
    var text = req.query.text;
    var color = req.query.color;

    console.log('got request to generate modelId='+modelId + ' text='+text + ' color='+color);
    // modelId = 'test1_color';

    var id = generator.generateSolidworks(modelId, text, color, function(id){
        if(!id){
            console.log('error, failed to generate ', req.originalUrl);
            res.send(JSON.stringify({id: id, status: 'error'}));
        } else {
            res.send(JSON.stringify({id: id, status: 'success'}));   
        }
    });
});

router.get('/generate_old', function(req, res, next){

    var modelId = req.query.modelId;
    var text = req.query.text;

    var id = generator.generate(modelId, text);
    res.send(JSON.stringify({id: id, status: 'success'}));
});
module.exports = router;
