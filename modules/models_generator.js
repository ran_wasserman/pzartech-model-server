/**
 * Created by ran on 8/19/15.
 */
var fs = require('fs-extra');
var config = require('config');
var path = require('path');
var exec = require('child_process').exec;
var spawnSync = require('child_process').spawnSync;
var execSync = require('child_process').execSync;

var outputPath = config.get("output_path");
var templatePath = config.get("template_path");

var executeSolidworksCommand = function(id, command, cb){
    console.log('running command: ', command);
    var child = exec(command, function(error, stdout, stderr){
        if(error){
            console.log('command failed', command);
            console.log('error: ' + error);
            console.log('stderr: ' + stderr);
            cb();
        } else {
            console.log('success, generated id:', id);
            cb(id);
        }
    });
}

var executeSolidworksCommandSync = function(id, command, cb, args){
    console.log('running command: ', command);
    // var res = spawnSync(command, {encoding: 'utf8'});
    var res = spawnSync(command, args, {encoding: 'utf8'});
    if(res.status != 0){
        console.log('command failed', command);
        console.log(res);
        console.log('error: ' + res.error);
        console.log('stderr: ' + res.stderr);
        cb();
    } else {
        console.log('success, generated id:', id);
        cb(id);
    }
}


var generator = {
    generate : function(modelId, text){
        var id = modelId + new Date().getTime();

        console.log('generating modelId', modelId, 'with text', text, 'id', id);

        var modelPathStl = path.join(templatePath, modelId) + '.stl';
        var outputStl = path.join(outputPath, id) + '.stl';
        console.log('about to copy', modelPathStl, outputStl);
        fs.copySync(modelPathStl, outputStl);

        var modelPathJpg = path.join(templatePath, modelId) + '.jpg';
        var outputJpg = path.join(outputPath, id) + '.jpg';
        console.log('about to copy', modelPathJpg, outputJpg);
        fs.copySync(modelPathJpg, outputJpg);

        return id;
    },

    generateSolidworks : function(modelId, text, color, cb) {
        var sketch = 'sketch5';
        var id = modelId + '_' + new Date().getTime();
        var temPath = path.resolve(templatePath, modelId+'.sldprt');
        // console.log('templatePath:', temPath);
        var outPath = path.resolve(outputPath, id);
        // console.log('outPath:', outPath);
        if(!color){
            color = '6668574';
        }
        var args = [temPath, sketch, text, color, outPath, 'jpg', 'stl', 'x_t']
        // var command = 'SW_simple_customization.exe '+ temPath + ' ' + sketch + ' "' + text + '" ' + outPath + ' jpg stl';
        var command = 'SW_simple_customization_color.exe';
        // console.log('command: ', command);
        
        executeSolidworksCommandSync(id, command, cb, args);
    }
};

module.exports = generator;